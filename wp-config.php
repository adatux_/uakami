<?php

define( 'BWPS_FILECHECK', true );

/**
* Configuración básica de WordPress.
*
* Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
* claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
* visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
* wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
*
* This file is used by the wp-config.php creation script during the
* installation. You don't have to use the web site, you can just copy this file
* to "wp-config.php" and fill in the values.
*
* @package WordPress
*/

define('WP_HOME','http://www.uakami.com');
define('WP_SITEURL','http://www.uakami.com');

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'uakami_site_wpml');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'uakami_site');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'uakami_site');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'gpc-www-prod.csjskjqdhauc.us-east-1.rds.amazonaws.com');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
* Claves únicas de autentificación.
*
* Define cada clave secreta con una frase aleatoria distinta.
* Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
* Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
*
* @since 2.6.0
*/
define('AUTH_KEY', 'co5.tMdv[8HNHo+5X<IPdEf.?/jR..~xR2hh)+lsp4kbJ+s05(#[+dwK2rl&r;KT'); // Cambia esto por tu frase aleatoria.
define('SECURE_AUTH_KEY', '1bAPHip@!y)i{Qy(%?47Sq @M|aA<H9J@?UqyPkgG0EkhMe o|dV?}~*kNGJ0TlZ'); // Cambia esto por tu frase aleatoria.
define('LOGGED_IN_KEY', '8uN4wB t+u[,$y/7gXkbnQ?6#76pkrzGXX>d5:.o!=F~_F,QN!agZ1V0|]/U5x|3'); // Cambia esto por tu frase aleatoria.
define('NONCE_KEY', 'e>t^sH7v,/S!j2`]h8pbc6/5{J$=x<re9yKQtqtLnDm>u6+*Ga1UK(K[ZU9*@UwB'); // Cambia esto por tu frase aleatoria.
define('AUTH_SALT', '<Y6dvM@J$}:^ 6o|h&-c(,,tMG,:7eW87(dq2e<Pvl{y)[T`Jb ?8&?BrG-__=z{'); // Cambia esto por tu frase aleatoria.
define('SECURE_AUTH_SALT', 'Np>G,{Q`u<*-N$t%n]YRO>HMGcKpd4TOj}2EOw:8;Jd>@$:~qN1QRubTgMs7{i@&'); // Cambia esto por tu frase aleatoria.
define('LOGGED_IN_SALT', 'A!k^{0[8jK9sX_1G;t4GvJ~mP0zU*0Ts6^KMUBr>6-wXaRpbXuPqI)%)1XN5Zs!}'); // Cambia esto por tu frase aleatoria.
define('NONCE_SALT', '2A{ln~!|CChYG0X87FHw#x7uI6I s}ji{@3H]T{`|m .|%qg1.wFJ3C/1h[brzq9'); // Cambia esto por tu frase aleatoria.

/**#@-*/

/**
* Prefijo de la base de datos de WordPress.
*
* Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
* Emplea solo números, letras y guión bajo.
*/
$table_prefix  = 'af6kbvtlkc_';

/**
* Idioma de WordPress.
*
* Cambia lo siguiente para tener WordPress en tu idioma. El correspondiente archivo MO
* del lenguaje elegido debe encontrarse en wp-content/languages.
* Por ejemplo, instala ca_ES.mo copiándolo a wp-content/languages y define WPLANG como 'ca_ES'
* para traducir WordPress al catalán.
*/
define('WPLANG', 'es_ES');

/**
* Para desarrolladores: modo debug de WordPress.
*
* Cambia esto a true para activar la muestra de avisos durante el desarrollo.
* Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
* en sus entornos de desarrollo.
*/
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
